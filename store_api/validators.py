from pydantic import BaseModel


class Message(BaseModel):
    message: str


class Category(BaseModel):
    name: str


class Product(BaseModel):
    id: int
    name: str
    description: str
    price: float
    image: str
    categories: list[Category]
