from pathlib import Path

from .env_settings import EnvSettings


ENV = EnvSettings()

# api settings

SECRET_KEY = ENV.API.SECRET_KEY
DEBUG = ENV.API.DEBUG
ALLOWED_HOSTS = ENV.API.ALLOWED_HOSTS
LANGUAGE_CODE = ENV.API.LANGUAGE_CODE
TIME_ZONE = ENV.API.TIME_ZONE

BASE_DIR = Path(__file__).resolve().parent
