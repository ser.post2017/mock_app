import json
import os
from typing import List

from fastapi import FastAPI, HTTPException

from .settings import BASE_DIR, ENV
from .validators import Category, Message, Product


app = FastAPI()
filepath = os.path.join(BASE_DIR, "products.json")
with open(filepath, "r") as f:
    db = json.load(f)
products = db.get("products")
categories = db.get("categories")


@app.get("/", response_model=Message)
async def get_config():
    message = {
        "message": f"""Success! api app initialized with settings:
        {ENV.API.dict()}"""
    }
    return message


@app.get("/products/", response_model=List[Product])
async def get_products():
    return products


@app.get("/products/{product_id}", response_model=Product)
async def get_product(product_id: int):
    for product in products:
        if product["id"] == product_id:
            return product
    raise HTTPException(status_code=404, detail="Product not found")


@app.post("/products/", response_model=Product)
async def create_product(product: Product):
    products.append(product.dict())
    return product


@app.put("/products/{product_id}", response_model=Product)
async def update_product(product_id: int, product: Product):
    for p in products:
        if p["id"] == product_id:
            p.update(product.dict())
            return p
    raise HTTPException(status_code=404, detail="Product not found")


@app.delete("/products/{product_id}")
async def delete_product(product_id: int):
    for i, p in enumerate(products):
        if p["id"] == product_id:
            del products[i]
            return {"message": "Product deleted successfully"}
    raise HTTPException(status_code=404, detail="Product not found")


@app.get("/categories/", response_model=List[Category])
async def get_categories():
    return categories


# Создание новой категории
@app.post("/categories/", response_model=Category)
async def create_category(category: Category):
    categories.append(category.dict())
    return category


# Удаление категории
@app.delete("/categories/{category_name}")
async def delete_category(category_name: str):
    for i, cat in enumerate(categories):
        if cat["name"] == category_name:
            del categories[i]
            return {"message": "Category deleted successfully"}
    raise HTTPException(status_code=404, detail="Category not found")
