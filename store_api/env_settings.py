from pydantic import BaseSettings


class ApiSettins(BaseSettings):
    SECRET_KEY: str = "empty"
    DEBUG: bool = False
    ALLOWED_HOSTS: str = "*"
    LANGUAGE_CODE: str = "ru-Ru"
    TIME_ZONE: str = "Europe/Moscow"


class EnvSettings(BaseSettings):
    API: ApiSettins = ApiSettins()
