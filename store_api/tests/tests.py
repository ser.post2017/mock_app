import pytest
from httpx import AsyncClient
from main import app


@pytest.fixture
async def client():
    async with AsyncClient(app=app, base_url="http://localhost/:8000") as client:
        yield client


@pytest.mark.asyncio
async def test_get_config(client: AsyncClient):
    response = await client.get("/")
    assert response.status_code == 200
    assert "api app initialized with settings" in response.json()["message"]


@pytest.mark.asyncio
async def test_get_products(client: AsyncClient):
    response = await client.get("/products/")
    assert response.status_code == 200
    assert isinstance(response.json(), list)


@pytest.mark.asyncio
async def test_get_product(client: AsyncClient):
    response = await client.get("/products/1")
    assert response.status_code == 200
    assert isinstance(response.json(), dict)


@pytest.mark.asyncio
async def test_create_product(client: AsyncClient):
    product_data = {
        "id": 11,
        "name": "Test Product",
        "description": "Test Description",
        "price": 10.0,
        "image": "test.jpg",
    }
    response = await client.post("/products/", json=product_data)
    assert response.status_code == 200
    assert response.json() == product_data
